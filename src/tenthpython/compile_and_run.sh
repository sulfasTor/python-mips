#! /bin/bash
path=$1
echo "-------------- PYTHON CODE ---------------------"
cat $path
echo "-------------- MIPS CODE -----------------------"
racket tenthpython.rkt $path
echo
echo "-------------- SPIM OUTPUT ---------------------"
path="$path.s"
spim -file $path | tail -n +2
echo
