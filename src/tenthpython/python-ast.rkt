;;; python-ast.rkt --- Declaration of the AST structures written in Racket

;; Copyright (C) 2018- Moises Torres Aguilar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
;; Author: Moises Torres Aguilar
;; URL: https://gitlab.com/sulfasTor/python-mips.git

#lang racket/base

(provide (all-defined-out))

(struct Pyvardef (id expr)	#:transparent)
(struct Pynum (value)		#:transparent)
(struct Pyname (value)		#:transparent)
(struct Pystr (value)		#:transparent)
(struct Pyop (op val1 val2)	#:transparent)
(struct Pyfactor (value sign) #:transparent)
