;;; stdlib.rkt --- Mips Standard Library functions

;; Copyright (C) 2018- Moises Torres Aguilar

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
;; Author: Moises Torres Aguilar
;; URL: https://gitlab.com/sulfasTor/python-mips.git

#lang racket/base

(provide (all-defined-out))

(struct Move (rd rs) #:transparent)
(struct Li (r i) #:transparent)
(struct La (r a) #:transparent)
(struct Addi (rs rd i) #:transparent)
(struct Add (rs rd) #:transparent)
(struct Div (rd rs) #:transparent)
(struct Mul  (loc a b) #:transparent)
(struct Remu (loc rd rs) #:transparent)
(struct Pow (x n) #:transparent)
(struct Sw (r loc) #:transparent)
(struct Lw (r loc) #:transparent)
(struct Syscall () #:transparent)
(struct Jr (r) #:transparent)
(struct B (r) #:transparent)
(struct Label (l) #:transparent)
(struct Comment (c) #:transparent)

;; addresses
(struct Data-lbl (l) #:transparent)
(struct Mem (b r) #:transparent)

;; Built-in functions (stdlib)

(define (mips-print-integer rd)
  (append
   (list
    (Comment "START PRINT INTEGER")
    (Move 'a0 rd)
    (Li 'v0 1)
    (Syscall))
   (list (Comment "END PRINT INTEGER"))
   (mips-print-new-line)))

(define (mips-print-string lbl)
  (list (Li 'v0 4)
        (La 'a0 (Data-lbl lbl))
        (Syscall)))

(define (mips-print-new-line)
  (append
   (list (Comment "START PRINT NEW LINE"))
   (mips-print-string 'nl)
   (list (Comment "END PRINT NEW LINE"))))

(define (mips-pow-pre)
  (list
   (Comment "START MIPS-POW")
   (Label 'loop_multiply)
   (Addi 't1 't1 -1)
   (Mul 't0 't0 't2)
   (B 'loop_multiply)
   (Label 'end_loop)
   (Move 'v0 't0)
   ;;(Li 'v0 1)
   ;;(Move 'a0 't0)
   ;;(Syscall)
   (Jr 'ra)
   (Comment "END MIPS-POW")))

(define (mips-pow x n)
  (Li 't0 x)
  (Li 't1 n)
  (B 'pow_multiply))
  
(define (mips-add v w)
  (list (Add v w)
        (Move 'v0 v)))

(define (mips-mult v w)
  (list (Mul 'v0 v w)))

(define (mips-div v w)
  (list (Div v w)
        (Move 'v0 v)))

(define (mips-remu v w)
  (list (Remu 'v0 v w)))
