.data
nl: .asciiz "\n"

.text
.globl main

#	START MIPS-POW

loop_multiply:
addi $t1, $t1, -1
mul $t0,$t0,$t2
b loop_multiply

end_loop:
move $v0, $t0
jr $ra

#	END MIPS-POW

main:
move $fp, $sp
li $v0, 3
addi $sp, $sp, -4
sw $v0, 0($sp)
li $v0, 4
addi $sp, $sp, -4
sw $v0, 0($sp)
add $t1, $t1, $t0
move $v0, $t0
li $v0, 3
addi $sp, $sp, -4
sw $v0, 0($sp)
li $v0, 7
addi $sp, $sp, -4
sw $v0, 0($sp)
mul $v0,$t0,$t1

#	START PRINT INTEGER
move $a0, $v0
li $v0, 1
syscall

#	END PRINT INTEGER

#	START PRINT NEW LINE
li $v0, 4
la $a0, nl
syscall

#	END PRINT NEW LINE
jr $ra
